import React from "react";
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
        <>
            <div>
                헤더
                <nev>
                    <Link to="/">여행 목록</Link>
                    |
                    <Link to="/wish">찜 목록</Link>
                </nev>
            </div>
            <main>{children}</main>
            <footer>footer</footer>
        </>
    )
}

export default DefaultLayout;