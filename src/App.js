import './App.css';
import Trip from "./components/Trip";
import {Routes, Route} from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";
import Wish from "./components/wish";



function App() {
  return (
    <div className="App">
        <Routes>
            <Route exact path="/" element={<DefaultLayout><Trip/></DefaultLayout>}></Route>
            <Route exact path="/wish" element={<DefaultLayout><Wish/></DefaultLayout>}></Route>
        </Routes>
    </div>
  );
}

export default App;
