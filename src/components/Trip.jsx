import React, {useEffect, useState} from 'react';

const Trip = () => {
    const [tripData , setTripData] = useState([]);
    // 초기 keyData 좋아요 눌린 항목 가져온다 ?
    const [keyData , setKeyData] = useState(JSON.parse(localStorage.getItem("keyData")) || []);

    useEffect(() => {
        // 전화를 걸 주소
        fetch("http://localhost:3000/trips.json")
            // 가져온 값을 객체로 변경
            .then(rsp => rsp.json())
            // setTripData 에 데이터 넣어주기
            .then(data => setTripData(data))
    }, []);

    useEffect(() => {
        // keyData , 문자로 바꾸고 로컬스토리지 올리기
        localStorage.setItem("keyData", JSON.stringify(keyData));
    }, [keyData]);


    const upDateStorage = (tripData) => {
        // 내가 클릭한 값이 스토리지에 있으면
        // some 조건에 맞으면 true 아니면 false 주는애
        const isInStorage = keyData.some(item => item.id === tripData.id);


        // 만약에 내가 클릭한 값이 스토리지에 있으면 -> 스토리지에 이미 있는 애들중에서 내가 누른 값이 같지않은 애들만 새로운 배열로 리턴해서 useState 에 주기
        if (isInStorage) {
            // keyData = 좋아요 누른 데이터 -> item
            // 좋아요 이미 누른애들으로 필터링을 하는데 조건이 내가 클릭한 id랑 이미 스토리지에 있는애들이랑 같지않아야한다
            const updatedKeyData = keyData.filter(item => item.id !== tripData.id);
            // 나머지 애들은 그대로 스토리지에 있어야하니까 같은애만 빼고
            // useState 에 넣으면 새로운 배열을 만들어서 중복을 제거 하니까
            setKeyData(updatedKeyData);

        } else {
            // 아직 스토리지에 없다면 추가
            setKeyData([...keyData, tripData]);
        }
    }




    return (
        <div>
            <h2 className="jo-h2"> 찜 목 록 </h2>
            {tripData.map((trip) => (
                <div key={trip.id}>
                    <div>{trip.name}</div>
                    <div>{trip.content}</div>
                    <div>
                        {/* keyData에 trip.id가 포함되어 있는지 확인 */}
                        {keyData.some(item => item.id === trip.id) ? <button onClick={() => upDateStorage(trip)}>❤️</button> : <button onClick={() => upDateStorage(trip)}>♡</button>}
                    </div>
                </div>
            ))}
        </div>
    )
}

export default Trip